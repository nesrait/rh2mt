
// FileQueue is the fix for "Error: EMFILE, too many open files"
// SEE http://unix.stackexchange.com/questions/81843/sudo-ulimit-command-not-found
// AND APPLY # ulimit -n 4096
var FileQueue = require('filequeue'),
    fs = new FileQueue(4096),
    xml2js = require('xml2js'),
    Promise = require("bluebird"),
    urlencode = require('urlencode'),
    request = require('superagent'),
    dateformat = require('dateformat');

var parser = new xml2js.Parser();

var sourceRoot = __dirname + '/../SS-ci';
var outputRoot = __dirname + '/out';
var outputPath = ['Service_Studio'];
fs.mkdir(outputRoot, function(){}, function(){});

// Content creation is documented here:
// https://success.mindtouch.com/Documentation/Integration/API/Pages/pages%2F%2F%7Bpageid%7D%2F%2Fcontents_(POST)
var createPage = function(path, node, data){
	var dekiwikiHost = '172.17.0.8';
	var parentPageName = path[path.length - 1];
	var pageName = node.name;
	var relativePageName = parentPageName + urlencode('/') + pageName;
	var now = new Date();
	var edittime = dateformat(now, 'yyyymmddhhMMss');
	var apiEndpoint = 'http://' + dekiwikiHost + '/@api/deki/pages/=' + urlencode(relativePageName) + '/contents?edittime='+ edittime;
	var shouldAbortIfExists = 'never';
	// Super useful:
	// https://visionmedia.github.io/superagent/
	return new Promise(function(ok, notok) {
		request
			.post(apiEndpoint)
			.auth('admin', 'outsystems')
			.type('text/plain')
			.set('Accept', 'application/json')
			.set('abort', shouldAbortIfExists)
			.send(data)
			.buffer()
			.on('response', function(res) {
				if (res.ok) {
					console.log(res.statusCode + ': ' + res.text);
					ok(res.text);
				} else {
					console.log(res.statusCode + ': ' + res.error.text)
					notok(res.error.text);
				}
	    	})
	  		.end();
	});
};

//createPage(['Service_Studio'], {name: 'Test'}, 'le body 3');

var migrateBook = function(path, node){
	return createPage(path, node, 'This is just a category.');

	// Skipping local for now
	return new Promise(function(ok, notok){
		var dirname = outputRoot + '/' + path.join('/') + '/' + node.name;
		fs.mkdir(dirname, function(error) {
			if (!error) {
				ok('DIRECTORY CREATED: ' + dirname);
				return;
			}
			ok('DIRECTORY EXISTED: ' + dirname);
		});
	});
};

// This will break on case-sensitive file systems
// Linux workaround (http://www.brain-dump.org/projects/ciopfs/,
//                   http://ubuntuforums.org/showthread.php?t=1436872)
// didn't work.
// Thor workaround: lowercase the input directories and files (http://stackoverflow.com/a/152741)
var readArticle = function(path, node) {
	return new Promise(function(ok, notok){
		var sourceFilename = sourceRoot + '/' + node.link.replace(/\\/g, '/').toLowerCase();
		fs.readFile(sourceFilename, function(err, data) {
			if (err) throw err;
			ok(data);
		});
	});
};

var writeArticle = function(path, node, data) {
	return createPage(path, node, data);

	// Skipping local for now
	return new Promise(function(ok, notok){
		fs.writeFile(filename, data, function(error) {
			if (!error) {
				ok('FILE CREATED: ' + filename);
				return;
			}
		});
	});
};

var migrateArticle = function(path, node){
	return new Promise(function(ok, notok){
		var filename = outputRoot + '/' + path.join('/') + '/' + node.name;
		fs.exists(filename, function (exists) {
			if (exists) {
				ok('FILE EXISTED: ' + filename);
				return;
			}
			readArticle(path, node).then(function(data) {
				var writeResult = writeArticle(path, node, data);
				ok(writeResult);
			});
		});
	});
};

var migrateNode = function(path, node){
	return new Promise(function(ok, notok){
		if (!node.link) {
			migrateBook(path, node).then(ok);
		} else {
			migrateArticle(path, node).then(ok);
		}
	}).then(function(res){
		console.log(res);
	});
};

var iterTree = function(path, depth, items) {
	return Promise.map(items, function(item){
		var node      = item.$ || {};
		node.children = item.item || [];
		return migrateNode(path, node).then(function(res){
			if (node.children.length > 0) {
				var childPath = path.slice();
				childPath.push(node.name);
				return iterTree(childPath, depth + 1, node.children);
			}
			return;
		});
	})
};

fs.readFile( sourceRoot + '/oss.hhc', function(err, data) {
    parser.parseString(data, function (err, result) {
    	if (result.toc && result.toc.item) {
    		var path = outputPath,
    		    depth = 0;
    		iterTree(path, depth, result.toc.item);	
    	}
    });
});
