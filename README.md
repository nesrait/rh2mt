Running the PoC
---------------

``` 
sudo docker run -it -p 80:80 nesrait/osdeki-ss
service dekiwiki start
```
   
Access via browser using the IPAddress of the last launched container:
```
sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' $(sudo docker ps -q)
```

Credentials:

  - Username: admin

  - Password: outsystems


Learning materials used to deliver this PoC
-------------------------------------------

 - Example RoboHelp project file template:
   http://blogs.adobe.com/techcomm/files/2012/10/FPJ_file_content.png

 - MindTouch / DekiWiki REST API for creating and updating pages:
   https://success.mindtouch.com/Documentation/Integration/API/Pages/pages%2F%2F%7Bpageid%7D%2F%2Fcontents_(POST)

 - DekiWiki in Docker
   https://hub.docker.com/r/nesrait/osdeki-ss/